package com.isoft.internship.calculator.utility;

import com.isoft.internship.calculator.model.Calculator;

import java.util.Scanner;

public class Menu {
    Calculator calculator = new Calculator();
    Scanner scanner = new Scanner(System.in);

    public void show(){
        System.out.println("Enter the first number:");
        double x = scanner.nextDouble();
        System.out.println("Enter the second number");
        double y = scanner.nextDouble();
        System.out.println("Choose an operation");
        System.out.println("1: +");
        System.out.println("2: -");
        System.out.println("3: *");
        System.out.println("4: /");
        System.out.println("Choice:");
        int choice = scanner.nextInt();
        handleOperation(choice, x, y);
    }

    public void handleOperation(int choice,double x, double y){
        switch (choice) {
            case 1:
                System.out.println(calculator.add(x,y));
                show();
                break;
            case 2:
                System.out.println(calculator.subtract(x,y));
                show();
                break;
            case 3:
                System.out.println(calculator.multiply(x,y));
                show();
                break;
            case 4:
                System.out.println(calculator.divide(x,y));
                show();
                break;
            default:
                System.out.println("\nInvalid option!");
                show();
                break;
        }
    }
}

