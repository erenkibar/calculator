package com.isoft.internship.calculator.interfaces;

import java.math.BigDecimal;

public interface IOperations {
    public double add(double x, double y);
    public double subtract(double x, double y);
    public double multiply(double x, double y);
    public double divide(double x, double y);
}