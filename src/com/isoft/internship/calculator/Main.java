package com.isoft.internship.calculator;

import com.isoft.internship.calculator.model.Calculator;
import com.isoft.internship.calculator.utility.Menu;

import java.math.BigDecimal;
import java.util.Scanner;

public class Main {
    public static void main(String args[]) {
        Menu menu = new Menu();
        menu.show();
    }
}
