package com.isoft.internship.calculator.model;

import com.isoft.internship.calculator.interfaces.IOperations;

import java.math.BigDecimal;

public class Calculator implements IOperations {

    @Override
    public double add(double x, double y) {
        return x + y;
    }

    @Override
    public double subtract(double x, double y) {
            return x - y;
        }
    @Override
    public double multiply(double x, double y) {
        return x * y;
    }

    @Override
    public double divide(double x, double y) {
        return x / y;
    }
}
